#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <string>

class FragTrap {

public:
	FragTrap();
	FragTrap(std::string name);
	FragTrap(FragTrap const& src);
	~FragTrap();
	
	void rangedAttack(const std::string& target);
	void meleeAttack(const std::string& target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	void vaulthunter_dot_exe(const std::string& target);

	std::string getName() const;
	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;

	FragTrap& operator=(const FragTrap& rhs);

private:
	std::string name_;
	unsigned int hitPoints_;
	unsigned int maxHitPoints_;
	unsigned int energyPoints_;
	unsigned int maxEnergyPoints_;
	unsigned int level_;
	unsigned int meleeAttackDamage_;
	unsigned int rangedAttackDamage_;
	unsigned int armorDamageReduction_;
};

#endif
