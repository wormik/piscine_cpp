#include "FragTrap.hpp"
#include <iostream>
#include <cstdlib> 

FragTrap::FragTrap()
	: name_("Jesus")
	, hitPoints_(100)
	, maxHitPoints_(100)
	, energyPoints_(100)
	, maxEnergyPoints_(100)
	, level_(1)
	, meleeAttackDamage_(30)
	, rangedAttackDamage_(20)
	, armorDamageReduction_(5)
{
	std::cout << this->getName() << ": Directive one: Protect humanity! Directive two: Obey Jack at all costs. Directive three: Dance!" << std::endl;
}

FragTrap::FragTrap(std::string name) 
	: name_(name)
	, hitPoints_(100)
	, maxHitPoints_(100)
	, energyPoints_(100)
	, maxEnergyPoints_(100)
	, level_(1)
	, meleeAttackDamage_(30)
	, rangedAttackDamage_(20)
	, armorDamageReduction_(5)
{
	std::cout << this->getName() << ": Look out everybody! Things are about to get awesome!" << std::endl;
}

FragTrap::FragTrap(const FragTrap& src)
{
	*this = src;
	std::cout << this->getName() << ": This time it'll be awesome, I promise!" << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << this->getName() << ": I'M DEAD I'M DEAD OHMYGOD I'M DEAD!" << std::endl;
}

void FragTrap::rangedAttack(const std::string& target)
{
	std::cout << "FR4G-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getRangedAttackDamage() << "> points of damage !" << std::endl;
}

void FragTrap::meleeAttack(const std::string& target)
{
	std::cout << "FR4G-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getMeleeAttackDamage() << "> points of damage !" << std::endl;
}

void FragTrap::takeDamage(unsigned int amount)
{
	if (this->hitPoints_ == 0) {
		std::cout << "FR4G-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else if (amount <= this->armorDamageReduction_) {
		std::cout << "FR4G-TP <" << this->getName() << "> received no damage due to his strong armor !" << std::endl;
	} else {
		if (this->hitPoints_ <= amount - this->armorDamageReduction_) {
			this->hitPoints_ = 0;
			std::cout << "FR4G-TP <" << this->getName() << "> has just died !" << std::endl;
		} else {
			this->hitPoints_ = this->hitPoints_ - amount + this->armorDamageReduction_;
			std::cout << "FR4G-TP <" << this->getName() << "> receives <" << amount << "> points of damage !" << std::endl;
			std::cout << "FR4G-TP <" << this->getName() << "> has <" << this->hitPoints_ << "> hitpoints !" << std::endl;
		}
	}
}

void FragTrap::beRepaired(unsigned int amount)
{
	if (this->hitPoints_ == this->maxHitPoints_) {
		std::cout << "FR4G-TP <" << this->getName() << "> has full health !" << std::endl;
	} else if (this->hitPoints_ == 0) {
		std::cout << "FR4G-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else {
		if (this->hitPoints_ + amount > this->maxHitPoints_) {
			this->hitPoints_ = this->maxHitPoints_;
			std::cout << "Overheal is not possible in this game !" << std::endl;
		} else {
			this->hitPoints_ += amount;
			std::cout << "FR4G-TP <" << this->getName() << "> has been healed for <" << amount <<"> hitpoints !" << std::endl;
		}
		std::cout << "FR4G-TP <" << this->getName() << "> has <" << this->hitPoints_ <<"> hitpoints !" << std::endl;
	}
}

void FragTrap::vaulthunter_dot_exe(std::string const& target)
{
	std::string randomAttacks[] = {"Chidori", "Amaterasu", "Rasengan", "Chibaku Tensei", "Tsukuyomi"}; 
    int i = std::rand() % 5; 

	if (this->energyPoints_ < 25) {
		std::cout << "FR4G-TP <" << this->getName() << "> has only <" << this->energyPoints_ <<"> energy !" << std::endl;
	} else {
		this->energyPoints_ -= 25;
		std::cout << "FR4G-TP <" << this->getName() << "> has used <" << randomAttacks[i] <<"> on " << target << " and has " << this->energyPoints_ << " energy left !" << std::endl;
	}
}

std::string FragTrap::getName() const
{
	return this->name_;
}

int FragTrap::getHitPoints() const
{
	return this->hitPoints_;
}

int FragTrap::getMaxHitPoints() const
{
	return this->maxHitPoints_;
}

int FragTrap::getEnergyPoints() const
{
	return this->energyPoints_;
}

int FragTrap::getMaxEnergyPoints() const
{
	return this->maxEnergyPoints_;
}

int FragTrap::getLevel() const
{
	return this->level_;
}

int FragTrap::getMeleeAttackDamage() const
{
	return this->meleeAttackDamage_;
}

int FragTrap::getRangedAttackDamage() const
{
	return this->rangedAttackDamage_;
}

int FragTrap::getArmorDamageReduction() const
{
	return this->armorDamageReduction_;
}

FragTrap& FragTrap::operator=(const FragTrap& rhs)
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs)
	{
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
