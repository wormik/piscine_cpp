#include <iostream>
#include "SuperTrap.hpp"

SuperTrap::SuperTrap() 
	: FragTrap()
	, NinjaTrap()
{
	this->name_ = "Super";
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 120;
	this->maxEnergyPoints_ = 120;
	this->level_ = 1;
	this->meleeAttackDamage_ = 60;
	this->rangedAttackDamage_ = 20;
	this->armorDamageReduction_ = 5;
	std::cout << this->getName() << ": My power is OVER 9000!!!!!" << std::endl;
}

SuperTrap::SuperTrap(std::string name)
	: FragTrap(name)
	, NinjaTrap(name)
{
	this->name_ = name;
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 120;
	this->maxEnergyPoints_ = 120;
	this->level_ = 1;
	this->meleeAttackDamage_ = 60;
	this->rangedAttackDamage_ = 20;
	this->armorDamageReduction_ = 5;
	std::cout << this->getName() << ": I have so much untapped power" << std::endl;
}

SuperTrap::SuperTrap(const SuperTrap& src)
{
	*this = src;
	std::cout << this->getName() << ": It's alive. Alive!!!" << std::endl;
}

SuperTrap::~SuperTrap()
{
	std::cout << this->getName() << ": death is just the beginning" << std::endl;
}

void SuperTrap::rangedAttack(const std::string& target)
{
	FragTrap::rangedAttack(target);
}

void SuperTrap::meleeAttack(const std::string& target)
{
	NinjaTrap::meleeAttack(target);
}

SuperTrap& SuperTrap::operator=(const SuperTrap& rhs)
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
