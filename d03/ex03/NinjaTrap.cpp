#include <iostream>
#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap()
	: ClapTrap()
{
	this->name_ = "Marian";
	this->hitPoints_ = 60;
	this->maxHitPoints_ = 60;
	this->energyPoints_ = 120;
	this->maxEnergyPoints_ = 120;
	this->level_ = 1;
	this->meleeAttackDamage_ = 60;
	this->rangedAttackDamage_ = 5;
	this->armorDamageReduction_ = 0;
	std::cout << this->getName() << ": Now I will dominate!" << std::endl;
}

NinjaTrap::NinjaTrap(std::string name) : ClapTrap(name)
{
	this->name_ = name;
	this->hitPoints_ = 60;
	this->maxHitPoints_ = 60;
	this->energyPoints_ = 120;
	this->maxEnergyPoints_ = 120;
	this->level_ = 1;
	this->meleeAttackDamage_ = 60;
	this->rangedAttackDamage_ = 5;
	this->armorDamageReduction_ = 0;
	std::cout << this->getName() << ": Heyyah!" << std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const& src)
{
	*this = src;
	std::cout << this->getName() << ": I'm flying! I'm really flying!" << std::endl;
}

NinjaTrap::~NinjaTrap()
{
	std::cout << this->getName() << ": No fair! I wasn't ready." << std::endl;
}

void NinjaTrap::rangedAttack(std::string const& target)
{
	std::cout << "N1NJ4-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getRangedAttackDamage() << "> points of damage !" << std::endl;
}

void NinjaTrap::meleeAttack(std::string const& target)
{
	std::cout << "N1NJ4-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getMeleeAttackDamage() << "> points of damage !" << std::endl;
}

void NinjaTrap::ninjaShoebox(NinjaTrap const& rhs)
{
	std::cout << this->getName() << " farts loudly in " << rhs.getName() << "'s face" << std::endl;
}

void NinjaTrap::ninjaShoebox(FragTrap const& rhs)
{
	std::cout << this->getName() << " spits right between " << rhs.getName() << "'s eyes" << std::endl;
}

void NinjaTrap::ninjaShoebox(ScavTrap const& rhs)
{
	std::cout << this->getName() << " shows his dirty, smelly crotch to " << rhs.getName() << std::endl;
}

NinjaTrap& NinjaTrap::operator=(NinjaTrap const& rhs)
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
