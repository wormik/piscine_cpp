#include "ScavTrap.hpp"
#include <iostream>
#include <cstdlib>

ScavTrap::ScavTrap() 
	: ClapTrap()
{
	this->name_ = "Judas";
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 50;
	this->maxEnergyPoints_ = 50;
	this->level_ = 1;
	this->meleeAttackDamage_ = 20;
	this->rangedAttackDamage_ = 15;
	this->armorDamageReduction_ = 3;

	std::cout << this->getName() << ": Glitching weirdness is a term of endearment, right?" << std::endl;
}

ScavTrap::ScavTrap(std::string name)
	: ClapTrap(name)
{
	this->name_ = name;
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 50;
	this->maxEnergyPoints_ = 50;
	this->level_ = 1;
	this->meleeAttackDamage_ = 20;
	this->rangedAttackDamage_ = 15;
	this->armorDamageReduction_ = 3;

	std::cout << this->getName() << ": Recompiling my combat code!" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap& src)
{
	*this = src;
	std::cout << this->getName() << ": Let's get this party started!" << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << this->getName() << ": Argh arghargh death gurgle gurglegurgle urgh... death." << std::endl;
}

void ScavTrap::rangedAttack(const std::string& target)
{
	std::cout << "SC4V-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getRangedAttackDamage() << "> points of damage !" << std::endl;
}

void ScavTrap::meleeAttack(const std::string& target)
{
	std::cout << "SC4V-TP <" << this->getName() << "> attacks <" << target << "> at melle, causing <" << this->getMeleeAttackDamage() << "> points of damage !" << std::endl;
}

void ScavTrap::challengeNewcomer()
{
	std::string challenges[] = {"Blood Bucket", "Raw Onion", "The Wasabi", "Dick in the Pic", "Hot Pepper"}; 
    int i = std::rand() % 5; 

	if (this->energyPoints_ < 25) {
		std::cout << "SC4V-TP <" << this->getName() << "> has only <" << this->energyPoints_ <<"> energy !" << std::endl;
	} else {
		this->energyPoints_ -= 25;
		std::cout << "SC4V-TP <" << this->getName() << "> challenges the newcomer in <" << challenges[i] << " Challenge> and has " << this->energyPoints_ << " energy left !" << std::endl;
	}
}

ScavTrap& ScavTrap::operator=(const ScavTrap& rhs)
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = getHitPoints();
		this->maxHitPoints_ = getMaxHitPoints();
		this->energyPoints_ = getEnergyPoints();
		this->maxEnergyPoints_ = getMaxEnergyPoints();
		this->level_ = getLevel();
		this->meleeAttackDamage_ = getMeleeAttackDamage();
		this->rangedAttackDamage_ = getRangedAttackDamage();
		this->armorDamageReduction_ = getArmorDamageReduction();
	}
	return *this;
}
