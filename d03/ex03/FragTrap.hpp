#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <string>
#include "ClapTrap.hpp"

class FragTrap : public ClapTrap {

public:
	FragTrap();
	FragTrap(std::string name);
	FragTrap(const FragTrap& src);
	~FragTrap();

	void rangedAttack(const std::string& target);
	void meleeAttack(const std::string& target);
	void vaulthunter_dot_exe(const std::string& target);

	FragTrap& operator=(const FragTrap& rhs);
};

#endif
