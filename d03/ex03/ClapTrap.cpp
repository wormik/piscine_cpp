#include "ClapTrap.hpp"
#include <iostream>

ClapTrap::ClapTrap() {
	this->name_ = "default name";
	this->hitPoints_ = 0;
	this->maxHitPoints_ = 0;
	this->energyPoints_ = 0;
	this->maxEnergyPoints_ = 0;
	this->level_ = 1;
	this->meleeAttackDamage_ = 0;
	this->rangedAttackDamage_ = 0;
	this->armorDamageReduction_ = 0;
	std::cout << "ClapTrap constructor is called" << std::endl;
}

ClapTrap::ClapTrap(const std::string name)
{
	this->name_ = name;
	this->hitPoints_ = 0;
	this->maxHitPoints_ = 0;
	this->energyPoints_ = 0;
	this->maxEnergyPoints_ = 0;
	this->level_ = 1;
	this->meleeAttackDamage_ = 0;
	this->rangedAttackDamage_ = 0;
	this->armorDamageReduction_ = 0;
	std::cout << "ClapTrap constructor with a name is called" << std::endl;
}

ClapTrap::ClapTrap(const ClapTrap& src)
{
	*this = src;
	std::cout << "Copy ClapTrap constructor is called" << std::endl;
}

ClapTrap::~ClapTrap()
{
	std::cout << "ClapTrap destructor is called" << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount)
{
	if (this->hitPoints_ == 0) {
		std::cout << "CLAP-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else if (amount <= this->armorDamageReduction_) {
		std::cout << "CLAP-TP <" << this->getName() << "> received no damage due to his strong armor !" << std::endl;
	} else {
		if (this->hitPoints_ <= amount - this->armorDamageReduction_) {
			this->hitPoints_ = 0;
			std::cout << "CLAP-TP <" << this->getName() << "> has just died !" << std::endl;
		} else {
			this->hitPoints_ = this->hitPoints_ - amount + this->armorDamageReduction_;
			std::cout << "CLAP-TP <" << this->getName() << "> receives <" << amount << "> points of damage !" << std::endl;
			std::cout << "CLAP-TP <" << this->getName() << "> has <" << this->hitPoints_ << "> hitpoints !" << std::endl;
		}
	}
}

void ClapTrap::beRepaired(unsigned int amount)
{
	if (this->hitPoints_ == this->maxHitPoints_) {
		std::cout << "CLAP-TP <" << this->getName() << "> has full health !" << std::endl;
	} else if (this->hitPoints_ == 0) {
		std::cout << "CLAP-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else {
		if (this->hitPoints_ + amount > this->maxHitPoints_) {
			this->hitPoints_ = this->maxHitPoints_;
			std::cout << "Overheal is not possible in this game !" << std::endl;
		} else {
			this->hitPoints_ += amount;
			std::cout << "CLAP-TP <" << this->getName() << "> has been healed for <" << amount <<"> hitpoints !" << std::endl;
		}
		std::cout << "CLAP-TP <" << this->getName() << "> has <" << this->hitPoints_ <<"> hitpoints !" << std::endl;
	}
}

std::string ClapTrap::getName() const
{
	return this->name_;
}

int ClapTrap::getHitPoints() const
{
	return this->hitPoints_;
}

int ClapTrap::getMaxHitPoints() const
{
	return this->maxHitPoints_;
}

int ClapTrap::getEnergyPoints() const
{
	return this->energyPoints_;
}

int ClapTrap::getMaxEnergyPoints() const
{
	return this->maxEnergyPoints_;
}

int ClapTrap::getLevel() const
{
	return this->level_;
}

int ClapTrap::getMeleeAttackDamage() const
{
	return this->meleeAttackDamage_;
}

int ClapTrap::getRangedAttackDamage() const
{
	return this->rangedAttackDamage_;
}

int ClapTrap::getArmorDamageReduction() const
{
	return this->armorDamageReduction_;
}

ClapTrap& ClapTrap::operator=(const ClapTrap& rhs)
{
	std::cout << "ClapTrap recompiling" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
