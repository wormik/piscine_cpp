#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP

#include <string>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap {

public:
	NinjaTrap();
	NinjaTrap(std::string name);
	NinjaTrap(const NinjaTrap& src);
	~NinjaTrap();

	void rangedAttack(const std::string& target);
	void meleeAttack(const std::string& target);
	void ninjaShoebox(const NinjaTrap& rhs);
	void ninjaShoebox(const ScavTrap& rhs); 
	void ninjaShoebox(const FragTrap& rhs);

	NinjaTrap& operator=(const NinjaTrap & rhs);
};

#endif
