#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <string>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap {

public:
	ScavTrap();
	ScavTrap(std::string name);
	ScavTrap(const ScavTrap& src);
	~ScavTrap();
	
	void rangedAttack(const std::string& target);
	void meleeAttack(const std::string& target);
	void challengeNewcomer();

	ScavTrap& operator=(const ScavTrap& rhs);
};

#endif
