#include "FragTrap.hpp"
#include <iostream>
#include <cstdlib>

FragTrap::FragTrap()
	: ClapTrap()
{
	this->name_ = "Jesus";
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 100;
	this->maxEnergyPoints_ = 100;
	this->level_ = 1;
	this->meleeAttackDamage_ = 30;
	this->rangedAttackDamage_ = 20;
	this->armorDamageReduction_ = 5;
	std::cout << this->getName() << ": Directive one: Protect humanity! Directive two: Obey Jack at all costs. Directive three: Dance!" << std::endl;
}

FragTrap::FragTrap(std::string name)
	: ClapTrap()
{
	this->name_ = name;
	this->hitPoints_ = 100;
	this->maxHitPoints_ = 100;
	this->energyPoints_ = 100;
	this->maxEnergyPoints_ = 100;
	this->level_ = 1;
	this->meleeAttackDamage_ = 30;
	this->rangedAttackDamage_ = 20;
	this->armorDamageReduction_ = 5;
	std::cout << this->getName() << ": Look out everybody! Things are about to get awesome!" << std::endl;
}

FragTrap::FragTrap(const FragTrap& src)
{
	*this = src;
	std::cout << this->getName() << ": This time it'll be awesome, I promise!" << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << this->getName() << ": I'M DEAD I'M DEAD OHMYGOD I'M DEAD!" << std::endl;
}

void FragTrap::rangedAttack(const std::string& target)
{
	std::cout << "FR4G-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getRangedAttackDamage() << "> points of damage !" << std::endl;
}

void FragTrap::meleeAttack(const std::string& target)
{
	std::cout << "FR4G-TP <" << this->getName() << "> attacks <" << target << "> at melle, causing <" << this->getMeleeAttackDamage() << "> points of damage !" << std::endl;
}

void FragTrap::vaulthunter_dot_exe(const std::string& target)
{
	std::string randomAttacks[] = {"Chidori", "Amaterasu", "Rasengan", "Chibaku Tensei", "Tsukuyomi"}; 
    int i = std::rand() % 5; 

	if (this->energyPoints_ < 25) {
		std::cout << "FR4G-TP <" << this->getName() << "> has only <" << this->energyPoints_ <<"> energy !" << std::endl;
	} else {
		this->energyPoints_ -= 25;
		std::cout << "FR4G-TP <" << this->getName() << "> has used <" << randomAttacks[i] <<"> on " << target << " and has " << this->energyPoints_ << " energy left !" << std::endl;
	}
}

FragTrap& FragTrap::operator=(const FragTrap& rhs) 
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
