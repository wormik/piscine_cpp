#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <string>

class ClapTrap {

public:

	ClapTrap();
	ClapTrap(const std::string name);
	ClapTrap(const ClapTrap& src);
	~ClapTrap();

	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	std::string getName() const;
	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;

	ClapTrap& operator=(const ClapTrap& rhs);

protected:

	std::string name_;
	unsigned int hitPoints_;
	unsigned int maxHitPoints_;
	unsigned int energyPoints_;
	unsigned int maxEnergyPoints_;
	unsigned int level_;
	unsigned int meleeAttackDamage_;
	unsigned int rangedAttackDamage_;
	unsigned int armorDamageReduction_;
};

#endif
