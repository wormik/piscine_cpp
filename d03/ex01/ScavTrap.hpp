#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <string>

class ScavTrap {

public:
	ScavTrap();
	ScavTrap(std::string name);
	ScavTrap(const ScavTrap& src);
	~ScavTrap();
	
	void rangedAttack(const std::string& target);
	void meleeAttack(const std::string& target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

	void challengeNewcomer();

	std::string getName() const;
	int getHitPoints() const;
	int getMaxHitPoints() const;
	int getEnergyPoints() const;
	int getMaxEnergyPoints() const;
	int getLevel() const;
	int getMeleeAttackDamage() const;
	int getRangedAttackDamage() const;
	int getArmorDamageReduction() const;

	ScavTrap& operator=(const ScavTrap& rhs);

protected:
private:
	std::string name_;
	unsigned int hitPoints_;
	unsigned int maxHitPoints_;
	unsigned int energyPoints_;
	unsigned int maxEnergyPoints_;
	unsigned int level_;
	unsigned int meleeAttackDamage_;
	unsigned int rangedAttackDamage_;
	unsigned int armorDamageReduction_;
};

#endif
