#include <cstdlib> 
#include <iostream>
#include "ScavTrap.hpp"

ScavTrap::ScavTrap()
	: name_("Judas")
	, hitPoints_(100)
	, maxHitPoints_(100)
	, energyPoints_(50)
	, maxEnergyPoints_(50)
	, level_(1)
	, meleeAttackDamage_(20)
	, rangedAttackDamage_(15)
	, armorDamageReduction_(3)
{
	std::cout << this->getName() << ": Glitching weirdness is a term of endearment, right?" << std::endl;
}

ScavTrap::ScavTrap(std::string name) 
	: name_(name)
	, hitPoints_(100)
	, maxHitPoints_(100)
	, energyPoints_(50)
	, maxEnergyPoints_(50)
	, level_(1)
	, meleeAttackDamage_(20)
	, rangedAttackDamage_(15)
	, armorDamageReduction_(3) 
{
	std::cout << this->getName() << ": Recompiling my combat code!" << std::endl;
}

ScavTrap::ScavTrap(const ScavTrap& src)
{
	*this = src;
	std::cout << this->getName() << ": Let's get this party started!" << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << this->getName() << ": Argh arghargh death gurgle gurglegurgle urgh... death." << std::endl;
}

void ScavTrap::rangedAttack(const std::string& target)
{
	std::cout << "SC4V-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getRangedAttackDamage() << "> points of damage !" << std::endl;
}

void ScavTrap::meleeAttack(const std::string& target)
{
	std::cout << "SC4V-TP <" << this->getName() << "> attacks <" << target << "> at range, causing <" << this->getMeleeAttackDamage() << "> points of damage !" << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount)
{
	if (this->hitPoints_ == 0) {
		std::cout << "SC4V-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else if (amount <= this->armorDamageReduction_) {
		std::cout << "SC4V-TP <" << this->getName() << "> received no damage due to his strong armor !" << std::endl;
	} else {
		if (this->hitPoints_ <= amount - this->armorDamageReduction_) {
			this->hitPoints_ = 0;
			std::cout << "SC4V-TP <" << this->getName() << "> has just died !" << std::endl;
		} else {
			this->hitPoints_ = this->hitPoints_ - amount + this->armorDamageReduction_;
			std::cout << "SC4V-TP <" << this->getName() << "> receives <" << amount << "> points of damage !" << std::endl;
			std::cout << "SC4V-TP <" << this->getName() << "> has <" << this->hitPoints_ << "> hitpoints !" << std::endl;
		}
	}
}

void ScavTrap::beRepaired(unsigned int amount)
{
	if (this->hitPoints_ == this->maxHitPoints_) {
		std::cout << "SC4V-TP <" << this->getName() << "> has full health !" << std::endl;
	} else if (this->hitPoints_ == 0) {
		std::cout << "SC4V-TP <" << this->getName() << "> is dead already !" << std::endl;
	} else {
		if (this->hitPoints_ + amount > this->maxHitPoints_) {
			this->hitPoints_ = this->maxHitPoints_;
			std::cout << "Overheal is not possible in this game !" << std::endl;
		} else {
			this->hitPoints_ += amount;
			std::cout << "SC4V-TP <" << this->getName() << "> has been healed for <" << amount <<"> hitpoints !" << std::endl;
		}
		std::cout << "SC4V-TP <" << this->getName() << "> has <" << this->hitPoints_ <<"> hitpoints !" << std::endl;
	}
}

void ScavTrap::challengeNewcomer()
{
	std::string challenges[] = {"Blood Bucket", "Raw Onion", "The Wasabi", "Dick in the Pic", "Hot Pepper"}; 
    int i = std::rand() % 5; 

	if (this->energyPoints_ < 25) {
		std::cout << "SC4V-TP <" << this->getName() << "> has only <" << this->energyPoints_ <<"> energy !" << std::endl;
	} else {
		this->energyPoints_ -= 25;
		std::cout << "SC4V-TP <" << this->getName() << "> challenges the newcomer in <" << challenges[i] << " Challenge> and has " << this->energyPoints_ << " energy left !" << std::endl;
	}
}

std::string ScavTrap::getName() const
{
	return this->name_;
}

int ScavTrap::getHitPoints() const
{
	return this->hitPoints_;
}

int ScavTrap::getMaxHitPoints() const
{
	return this->maxHitPoints_;
}

int ScavTrap::getEnergyPoints() const
{
	return this->energyPoints_;
}

int ScavTrap::getMaxEnergyPoints() const
{
	return this->maxEnergyPoints_;
}

int ScavTrap::getLevel() const
{
	return this->level_;
}

int ScavTrap::getMeleeAttackDamage() const
{
	return this->meleeAttackDamage_;
}

int ScavTrap::getRangedAttackDamage() const
{
	return this->rangedAttackDamage_;
}

int ScavTrap::getArmorDamageReduction() const
{
	return this->armorDamageReduction_;
}

ScavTrap& ScavTrap::operator=(const ScavTrap& rhs)
{
	std::cout << "Recompiling my combat code!" << std::endl;

	if (this != &rhs) {
		this->name_ = rhs.getName();
		this->hitPoints_ = rhs.getHitPoints();
		this->maxHitPoints_ = rhs.getMaxHitPoints();
		this->energyPoints_ = rhs.getEnergyPoints();
		this->maxEnergyPoints_ = rhs.getMaxEnergyPoints();
		this->level_ = rhs.getLevel();
		this->meleeAttackDamage_ = rhs.getMeleeAttackDamage();
		this->rangedAttackDamage_ = rhs.getRangedAttackDamage();
		this->armorDamageReduction_ = rhs.getArmorDamageReduction();
	}
	return *this;
}
