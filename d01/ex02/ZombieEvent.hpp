#ifndef ZOMBIE_EVENT_HPP
#define ZOMBIE_EVENT_HPP
#include <string>
#include "Zombie.hpp"

class ZombieEvent
{
public:

	ZombieEvent();
	~ZombieEvent();

	void setZombieType(std::string type);
	Zombie* newZombie(std::string name);
	void randomChump();

private:
	std::string type_;
};

#endif
