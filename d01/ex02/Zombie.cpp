#include "Zombie.hpp"
#include <iostream>

Zombie::Zombie(std::string& name, std::string& type)
	: name_(name)
	, type_(type)
{
}

Zombie::~Zombie()
{
}

void Zombie::announce()
{
	std::cout << "<" << name_ << " (" << type_ << ")> Braiiiiiiiinnnssss..." << std::endl;
}
