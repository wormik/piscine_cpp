#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int main()
{
	ZombieEvent factory;

	factory.randomChump();
	factory.setZombieType("crawler");
	factory.randomChump();
	factory.randomChump();
	factory.randomChump();

	Zombie* zed = factory.newZombie("Rudolph");
    zed->announce();
    delete zed;

	return 0;
}
