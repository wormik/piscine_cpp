#include "ZombieEvent.hpp"
#include <ctime>
#include <cstdlib>
#include <iostream>

ZombieEvent::ZombieEvent()
{
	type_ = "standard";
	std::srand(std::time(0));
}

ZombieEvent::~ZombieEvent()
{}

void ZombieEvent::setZombieType(std::string type)
{
	type_ = type;
}

Zombie* ZombieEvent::newZombie(std::string name)
{
	return (new Zombie(name, type_));
}

void ZombieEvent::randomChump()
{
	std::string zombiePool[5] = 
	{
		"il",
		"ar",
		"ap",
		"vz",
		"ps",
	};

	int i = std::rand() % 5;
	Zombie res = Zombie(zombiePool[i], type_);
	res.announce();
}
