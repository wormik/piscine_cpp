#include "HumanA.hpp"
#include <iostream>

HumanA::HumanA(std::string name, Weapon& weapon)
	: name_(name)
	, weapon_(weapon)
{
	std::cout << "HumanA (" << name_ << ") born" << std::endl;
}

HumanA::~HumanA()
{
	std::cout << "HumanA (" << name_ << ") died" << std::endl;
}

void HumanA::attack()
{
	std::cout << name_ << " attacks with his " << weapon_.getType() << std::endl;
}
