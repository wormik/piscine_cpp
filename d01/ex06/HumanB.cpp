#include "HumanB.hpp"
#include <iostream>

HumanB::HumanB(std::string name) 
	: name_(name)
{
	std::cout << "HumanB (" << name_ << ") has been born" << std::endl;
}

HumanB::~HumanB()
{
	std::cout << "HumanB (" << name_ << ") has died" << std::endl;
}

void HumanB::attack()
{
	std::cout << name_ << " attacks with his " << weapon_->getType() << std::endl;
}

void HumanB::setWeapon(Weapon &weapon)
{
	weapon_ = &weapon;
}
