#include "Weapon.hpp"
#include <iostream>

Weapon::Weapon(std::string type)
	: type_(type)
{
	std::cout << "The weapon has been created" << std::endl;
}

Weapon::~Weapon()
{
	std::cout << "The weapon has been destroyed" << std::endl;
}

const std::string& Weapon::getType()
{
	return type_;
}

void Weapon::setType(std::string type)
{
	type_ = type;
}