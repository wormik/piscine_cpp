#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP
#include <string>

class Zombie
{
public:

	Zombie();
	~Zombie();
	void announce();

	std::string name_;
	std::string type_;
};

#endif
