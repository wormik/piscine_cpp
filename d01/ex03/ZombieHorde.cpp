#include "ZombieHorde.hpp"
#include "Zombie.hpp"
#include <cstdlib>
#include <iostream>

ZombieHorde::ZombieHorde(int N)
	: num_of_zombies_(N)
{
	std::string zombieNames[5] = 
	{
		"il",
		"ar",
		"ap",
		"vz",
		"ps",
	};

	std::string zombieTypes[5] = 
	{
		"crawler",
		"berserk",
		"bloater",
		"punisher",
		"Dr.X"
	};
									
	horde_ = new Zombie[num_of_zombies_];
	for (int i = 0; i < num_of_zombies_; ++i) {
		horde_[i].name_ = zombieNames[rand() % 5];
		horde_[i].type_ = zombieTypes[rand() % 5];
	}
	std::cout << "Arrrrghhh..." << std::endl;
}

ZombieHorde::~ZombieHorde()
{
	delete [] horde_;
	std::cout << "Brglllua..." << std::endl;
}

void ZombieHorde::announce()
{
	for (int i = 0; i < num_of_zombies_; ++i) {
		horde_[i].announce();
	}
}
