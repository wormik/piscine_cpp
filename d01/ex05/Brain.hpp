#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <string>

class Brain
{

public:

	Brain(int weight, int age);
	~Brain();

	std::string identify() const;

	int weight_;
	int age_;
};

#endif
