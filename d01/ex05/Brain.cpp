#include "Brain.hpp"
#include <iostream>
#include <string>
#include <sstream>

Brain::Brain(int weight, int age)
	: weight_(weight)
	, age_(age)
{
	std::cout << "Brain constructed" << std::endl;
}

Brain::~Brain()
{
	std::cout << "~Brain destructed" << std::endl;
}

std::string Brain::identify() const
{
	std::stringstream stream;

	stream << this;
	return stream.str();
}
