#include "Human.hpp"
#include <iostream>
#include <string>
#include <iostream>

Human::Human() 
	: brain_(4, 23)
{
	std::cout << "Human created. lol" << std::endl;
}

Human::~Human()
{
	std::cout << "~Human horribly died" << std::endl;
}

const Brain& Human::getBrain()
{
	return (brain_);
}


std::string Human::identify()
{
	return (brain_.identify());
}