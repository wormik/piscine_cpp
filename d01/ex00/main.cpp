#include <iostream>
#include "Pony.hpp"

void ponyOnTheHeap()
{
	std::cout << __func__ << ": ";
	Pony *pony;
	pony = new Pony(42);
	pony->describe();
	delete pony;
}

void ponyOnTheStack()
{
	std::cout << __func__ << ": ";
	Pony pony(0);
	pony.describe();
}

int main()
{
	ponyOnTheHeap();
	ponyOnTheStack();
	Pony::print_total();

	return (0);
}
