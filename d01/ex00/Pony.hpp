#ifndef PONY_HPP
#define PONY_HPP

class Pony
{
public:

	Pony();
	Pony(int num);
	~Pony();

	void describe();
	static void print_total();
	
private:
	int number_of_legs;
	static int total_ponies;
};

#endif
