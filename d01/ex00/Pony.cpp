#include "Pony.hpp"
#include <iostream>

int Pony::total_ponies = 0;

Pony::Pony()
	: number_of_legs(0)
{
	total_ponies += 1;
	std::cout << "Pony created. Total:" << total_ponies << std::endl; 
}

Pony::Pony(int num)
	: number_of_legs(num)
{
	total_ponies += 1;
	std::cout << "Pony created. Total:" << total_ponies << std::endl; 
}

Pony::~Pony()
{
	total_ponies -= 1;
	std::cout << "~Pony deleted. Total:" << total_ponies << std::endl; 
}

void Pony::describe()
{
	std::cout << "this pony has " << number_of_legs << " legs" << std::endl;
}

void Pony::print_total()
{
	std::cout << "Total ponies:" << total_ponies << std::endl;
}