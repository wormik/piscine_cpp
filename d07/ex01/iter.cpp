#include <iostream>

template <typename T>
void iter(const T* arr, int size, void (*func)(const T&))
{
	for (int i = 0; i < size; ++i)
		func(arr[i]);
}

template <typename T>
void foo(const T& smth)
{
	std::cout << smth << std::endl;
}

int main()
{
	int			arr1[] = {42, 69, 23, 100500, 12345};
	std::string	arr2[] = {"these", "are", "strings"};
	float		arr3[] = {12.3, 45.6, 78.9, 10.11};

	iter(arr1, 5, foo);
	std::cout << std::endl;

	iter(arr2, 3, foo);
	std::cout << std::endl;

	iter(arr3, 4, foo);
	std::cout << std::endl;

	return 0;
}
