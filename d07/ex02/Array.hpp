#include <iostream>

template <typename T = int>
class Array
{
public:
	Array<T>() : _arr(0), _size(0)
	{}

	Array<T>(unsigned int n) : _size(n)
	{
		if (_size != 0) {
			_arr = new T[n];
		} else {
			_arr = 0;
		}
	}

	Array<T>(const Array<T>& src) : _arr(0), _size(0)
	{
		*this = src;
	}

	~Array<T>()
	{
		if (_arr) {
			delete [] _arr;
		}
	}

	unsigned int size() const
	{
		return this->_size;
	}

	Array<T>& operator=(const Array<T>& rhs)
	{
		if (this != &rhs) {
			if (_arr) {
				delete [] this->_arr;
				_arr = 0;
			}
			_size = rhs.size();
			if (_size != 0) {
				_arr = new T[_size];
				for (unsigned int i = 0; i < _size; ++i) {
					_arr[i] = rhs._arr[i];
				}
			}
		}
		return *this;
	}

	T& operator[](unsigned int position)
	{
		if (position >= _size) {
			throw std::exception();
		} else {
			return _arr[position];
		}
	}

private:
	T* _arr;
	unsigned int _size;
};
