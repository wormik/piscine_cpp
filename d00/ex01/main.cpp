#include <iostream>
#include <iomanip>
#include "Contact.hpp"

int choose_contact(int size)
{
	int choice = -1;
	std::string str;

	while (!std::cin.eof())
	{
		std::cout << "Index:";
		std::getline(std::cin, str);
		if (!str.empty()) {
			choice = std::stoi(str);
		}
		str.clear();
		if (0 <= choice && choice <  size)
			return (choice);
	}
	return (choice);
}

std::string menu()
{
	std::string choice;

	std::cout << "1. ADD" << std::endl;
	std::cout << "2. SEARCH" << std::endl;
	std::cout << "3. EXIT" << std::endl;
	std::cout << "Choice:";
	std::getline(std::cin, choice);
	
	return (choice);
}

int main()
{
	const int max_size = 8;
	std::string choice;
	int size = 0;
	int to_show;
	Contact contacts[max_size];

	while (!std::cin.eof())
	{
		choice = menu();
		if (choice == "ADD" || choice == "1")
		{
			if (size == max_size)
			{
				std::cout << "Can not add more than 8 contacts" << std::endl;
			}
			else
			{
				contacts[size].add_all();
				size++;
			}
		}
		else if (choice == "SEARCH" || choice == "2")
		{
			Contact::print_header();
			for (int i = 0; i < size; i++)
			{
				contacts[i].print_short(i);
			}
			to_show = choose_contact(size);
			contacts[to_show].print_full();
		}
		else if (choice == "EXIT" || choice == "3")
		{
			return (0);
		}
		else
		{
			std::cout << "Invalid option" << std::endl;
		}
	}

	return (0);
}
