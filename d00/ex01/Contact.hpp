#ifndef CONTACT_HPP
#define CONTACT_HPP
#include <string>

class Contact
{
public:

	Contact();
	~Contact();
	void add_all();
	void print_short(int index);
	void print_full();
	static void print_header();

	std::string first_name;
	std::string last_name;
	std::string nickname;
	std::string login;
	std::string postal_address;
	std::string email;
	std::string phone_number;
	std::string birthday_date;
	std::string favorite_meal;
	std::string underwear_color;
	std::string darkest_secret;

private:

	void add_field(std::string& field, const char* field_name);
	void print_field(const std::string& field);
};

#endif
