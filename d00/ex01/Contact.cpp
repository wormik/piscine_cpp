#include "Contact.hpp"
#include <iostream>
#include <iomanip>

Contact::Contact()
{}

Contact::~Contact()
{}

void Contact::add_all()
{
	add_field(first_name, "first_name");
	add_field(last_name, "last_name");
	add_field(nickname, "nickname");
	add_field(login, "login");
	add_field(postal_address, "postal_address");
	add_field(email, "email");
	add_field(phone_number, "phone_number");
	add_field(birthday_date, "birthday_date");
	add_field(favorite_meal, "favorite_meal");
	add_field(underwear_color, "underwear_color");
	add_field(darkest_secret, "darkest_secret");
}


void Contact::add_field(std::string& field, const char* field_name)
{
	std::cout << "Enter " << field_name << std::endl;
	std::getline(std::cin, field);
}

void Contact::print_header()
{
	std::cout << std::setw(11) << "index|" 
			  << std::setw(11) << "first_name|"
			  << std::setw(11) << "last_name|"
			  << std::setw(10) << "nickname" << std::endl;
	std::cout << "1234567890|1234567890|1234567890|1234567890" << std::endl;
}

void Contact::print_short(int index)
{

	std::cout << std::setw(10) << index << "|";
	print_field(first_name);		std::cout << "|";
	print_field(last_name);			std::cout << "|";
	print_field(nickname);			std::cout << std::endl;
}

void Contact::print_field(const std::string& field)
{
	if (field.size() > 10)
		std::cout << field.substr(0, 9) << ".";
	else if (field.size() == 0)
		std::cout << std::setw(10) << "empty";
	else
		std::cout << std::setw(10) << field;
}

void Contact::print_full()
{
	std::cout << "     first_name: " << first_name << std::endl;
	std::cout << "      last_name: " << last_name << std::endl;
	std::cout << "       nickname: " << nickname << std::endl;
	std::cout << "          login: " << login << std::endl;
	std::cout << " postal_address: " << postal_address << std::endl;
	std::cout << "          email: " << email << std::endl;
	std::cout << "   phone_number: " << phone_number << std::endl;
	std::cout << "  birthday_date: " << birthday_date << std::endl;
	std::cout << "  favorite_meal: " << favorite_meal << std::endl;
	std::cout << "underwear_color: " << underwear_color << std::endl;
	std::cout << " darkest_secret: " << darkest_secret << std::endl;
}
